use strict;
use Getopt::Long qw(GetOptions);
use Data::Dumper;
 
my $so=$ARGV[0];
my $de=$ARGV[1];
my $source_address;
GetOptions('o=s' => \$so,'d=s' => \$de,) or die "Usage: $0 --from NAME\n";

my $filename1=qq{/SANStorage/techusers/atripath/};
my $file1 = '/SANStorage/techusers/atripath/report_abhi.txt';
my @a=('p1','p2','p3');

my $c=1;
open(my $fhh, '>', $file1) or die "Could not open file '$file1' $!";
foreach(@a)
{

	$filename1=qq{/SANStorage/techusers/atripath/$_.txt};

	open(my $fh, '<', $filename1)  or die "Could not open file '$filename1' $!";
	
	while (my $row = <$fh>) {

		if($row ne '')
		{	
		
		next if($row=~m/Origin/gi && $c > 1);

    	chomp $row;
	$row =~s/[|,]/~/g;
	$row =~s/[-]/\//g;
	
	print $fhh "$row\n";

			$c++;	
  } 
}
}


`uniq report_abhi.txt`;

my $hash={};
my $ufile1=qq{/SANStorage/techusers/atripath/report_abhi.txt};
open(my $fhhh, '<', $ufile1) or die "Could not open file '$file1' $!";
while (my $row1 = <$fhhh>) 
{
	 chomp $row1;
 	my ($origin,$DepartureTime,$Destination,$DestinationTime,$Price)=split('~',$row1);
	my $key =qq{$origin}.qq{-}.qq{$Destination};
	if(exists $hash->{$key})
	{	
		push @{$hash->{$key}},{DepartureTime=>qq{$DepartureTime},DestinationTime=>qq{$DestinationTime},Price=>qq{$Price}}; 
	}else
	{
		$hash->{$key}=[{DepartureTime=>qq{$DepartureTime},DestinationTime=>qq{$DestinationTime},Price=>qq{$Price}}];
	}

}

if($so && $de)
{
my $ask=qq{$so}.qq{-}.qq{$de};

my $reqdata=$hash->{$ask};
if(exists $hash->{$ask} && scalar @$reqdata)
{

	foreach(sort {$a->{Price} cmp $b->{Price} or $a->{DepartureTime} cmp $b->{DepartureTime} } @$reqdata)
	{
		print qq{ $so --> $de ($_->{DepartureTime} --> $_->{DestinationTime}) - $_->{Price} \n};		
		
	}
		
}else
{
	print "No Flights Found for {$so} --> {$de}";
}

}
else
{
        print "please provide data";
}

